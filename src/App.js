import React from 'react';
import {ProviderLogin} from '@inrupt/solid-react-components';
import {LoggedIn, LoggedOut, LogoutButton} from '@solid/react';
import './App.css';
import './index.css';
import UserInfo from './components/UserInfo/userinfo.component';
import EducationInfo from './components/EducationInfo/educationinfo.component';
import WorkInfo from './components/WorkInfo/workinfo.component';
import SkillsInfo from './components/SkillsInfo/skillsinfo.component';
import CompanyInfo from './components/CompanyInfo/companyinfo.component';
import JobListings from './components/JobListings/joblistings.component';


export default function App() {

  return( 
    <div>
      <LoggedOut>
        <div className='loggedout'>
          <h2>Please login:</h2>
          <ProviderLogin callbackUri={`${window.location.origin}/`}/>
        </div>
      </LoggedOut>

      <LoggedIn>
        <div className='loggedin'>
          <UserInfo/>
          <EducationInfo/>
          <WorkInfo/>
          <SkillsInfo/>
          <CompanyInfo/>
          <JobListings/>
          <h2>Log out:</h2>
          <LogoutButton/>
        </div>
      </LoggedIn>
    </div>
  );
}
