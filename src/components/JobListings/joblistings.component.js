import React from 'react';
import {Spinner, FormModel} from '@inrupt/solid-react-components';
import {useWebId} from '@solid/react';

const joblistings = () => {
    const mywebid = useWebId();

    return(
        <div>
            <h2>Job listings:</h2>
            <FormModel
                {...{
                    modelSource: 'https://nikosiltaloppi.solid.community/public/CVTurtleFiles/CompanyJobListing.ttl#formRoot',
                    dataSource: mywebid,
                    options: {
                    theme: {
                        inputText: 'sdk-input',
                        input: 'skd-textinput',
                        form: 'sdk-form',
                        inputTextArea: 'sdk-textarea',
                        inputCheckbox: 'sdk-checkbox checkbox',
                        childGroup: 'inrupt-form-group',
                        groupField: 'group-wrapper',
                        multipleField: 'multiple-wrapper'
                    },
                    autosaveIndicator: Spinner,
                    autosave: true
                    }
                }}
            />
      </div>    
    );
};

export default joblistings;