import React from 'react';
import {Spinner, FormModel, Uploader, ProfileUploader} from '@inrupt/solid-react-components';
import {useWebId, Image} from '@solid/react';
import data from '@solid/query-ldflex';
import { namedNode } from '@rdfjs/data-model';



const userinfo = () => {
    const mywebid = useWebId();
    const defaultProfilePhoto = '/img/icon/empty-profile.svg';
    const updatePhoto = async (uri: String) => {
        try {
          const { user } = data;
          await user.vcard_hasPhoto.set(namedNode(uri));
        } catch (error) {
          console.log(error);
        }
      };
    return(
        <div>
            <h2>Basic information:</h2>
            <FormModel
                {...{
                    modelSource: 'https://nikosiltaloppi.solid.community/public/CVTurtleFiles/CVBasicInformation.ttl#formRoot',
                    dataSource: mywebid,
                    options: {
                    theme: {
                        inputText: 'sdk-input',
                        input: 'skd-textinput',
                        form: 'sdk-form',
                        inputTextArea: 'sdk-textarea',
                        inputCheckbox: 'sdk-checkbox checkbox',
                        childGroup: 'inrupt-form-group',
                        groupField: 'group-wrapper',
                        multipleField: 'multiple-wrapper'
                    },
                    autosaveIndicator: Spinner,
                    autosave: true
                    }
                }}
            />
            <h3>Add picture:</h3>
            <Image src="user.vcard_hasPhoto" defaultSrc={defaultProfilePhoto} className="pic"/>
            <Uploader
            {...{
            fileBase: mywebid && mywebid.split('/card')[0],
            accept: 'png,jpeg,jpg',
            onComplete: uploadedFiles => {
                updatePhoto(uploadedFiles[uploadedFiles.length - 1].uri);
            },
            render: props => <ProfileUploader {...{ ...props }} />
            }}
        />
      </div>    
    );
};

export default userinfo;