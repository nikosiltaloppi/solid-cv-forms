import React from 'react';
import {Spinner, FormModel} from '@inrupt/solid-react-components';
import {useWebId} from '@solid/react';

const educationinfo = () => {
    const mywebid = useWebId();

    return(
        <div>
            <h2>Education:</h2>
            <FormModel
                {...{
                    modelSource: 'https://nikosiltaloppi.solid.community/public/CVTurtleFiles/CVEducation.ttl#formRoot',
                    dataSource: mywebid,
                    options: {
                    theme: {
                        inputText: 'sdk-input',
                        input: 'skd-textinput',
                        form: 'sdk-form',
                        inputTextArea: 'sdk-textarea',
                        inputCheckbox: 'sdk-checkbox checkbox',
                        childGroup: 'inrupt-form-group',
                        groupField: 'group-wrapper',
                        multipleField: 'multiple-wrapper'
                    },
                    autosaveIndicator: Spinner,
                    autosave: true
                    }
                }}
            />
      </div>    
    );
};

export default educationinfo;